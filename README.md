# Simple Sample DApp: Ethereum + IPFS + React.js V1.1

A simple reference DApp to upload a document to IPFS (InterPlanetary File System) and then store the IPFS hash on the Ethereum blockchain. Once the IPFS hash number is sent to the Ethereum blockchain, the user will receive a transaction receipt. 

We will use Create-React-App framework to make a front-end. This Dapp works with any user that has MetaMask installed in their browser.

You may consider to use IPFS and Ethereum for those use cases where documents need to be persisted and to ensure it is not tempered. This simple app just provides a starting point for you to explore how you can prototype your use cases using IPFS and Ethereum.

The sample dapp will run in the CDE on the ML BPaaS platform.

you can access the README on https://gitlab.com/brucelu2018-dev/eth-ipfs-sample/blob/master/README.md


## Explore and run the DApp

1. Install MetaMask chrome browser extension. If do not have MetaMask installed, please go to https://metamask.io/ and follow the instructions. This DApp will assume the user has MetaMask installed.

2. Download the Ethereum and IPFS Sample App from the Application Library into My Repository which will create a repository in your GitLab.  For convenience, you can set the repository as "Public" from My Repository. Then create a new CDE workspace from the repository that is created with the app downloaded. Then use the workspace to explore and test the sample application. Refer to the platform document: https://docs.morpheuslabs.io/docs for how to download app, how to access my repository and how to create a new workspace.

3. Deploy smart contract to your local network in the terminal or a POA network on the platform (or ropsten testnet). You can issue the following command in a terminal of CDE.

Start a local network (ganache). This start the ganache on 127.0.0.1:8545
`ganache-cli -h 0.0.0.0`

Deploy the smart contracts to the local network (configure "host" and "port" in truffle-config.js using the CDE code editor tool). Open a new terminal in CDE and run the following command

`cd ethereum`

`npmtruffle migrate`

Get StoreHash smart contract address and paste it to the address field in `src/storehash.js` (you can do this using the CDE code editor).

4. Connect Metamask to local RPC network by getting url of ganache from servers tab. Create a new wallet using an account's private key provided by Ganache (it is shown when you started the ganache)

5. In the terminal, run `npm install` to install node.js modules

6. In the terminal, run dev server `npm start` . You can find out the url of "node" of "servers" in CDE, click on the URL will launch the sample applicaiton in browser, then can try to upload a file into IPFS and receive a IPFS hash and Ethereum transaction hash.


## Highlights

### Access IPFS

In ipfs.js in the "src" folder, the following are used to access IPFS testnet via infura

`const ipfsClient = require('ipfs-http-client');`

`const ipfs = ipfsClient('ipfs.infura.io', '5001', { protocol: 'https' })`



